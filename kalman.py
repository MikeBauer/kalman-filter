'''
Created on 22 Feb. 2018

@author: Michael

Situation: A train needs to be positioned along a track using inertial data.
Position is in meters, time in seconds.
'''
import random, numpy as np
import matplotlib.pyplot as plt
import time

def noise(mean=0,sd=0):
    """
        @param mean: The mean value of the gaussian
        @type mean: float
        @param sd: The standard deviation of the gaussian. 
                    The range from mean-sd to mean+sd should cover 68% of the measurements.
        @return: A single random value following a gaussian with the given mean and standard deviation
        @rtype: float
    """
    return random.gauss(mean,sd)

def make_gps_noisy(state,sd=0):
    """
        Utilizes the noise() function in a convenient manner for 
        vector like states (not the State class)
    """
    return (state[0]+noise(sd=sd),state[1]+noise(sd=sd))
    
    
def generate_gps(initState, acceleration, time):
    """
        Generates A list of states, one for each time step based on acceleration data
        I purely use this to generate the data and has nothing to do with the kalman filter
        It's far easier to generate GPS positions this way and picture the acceleration 
        of the train rather than provide arbitrary positions that may not be consistent 
        without a good deal of planning.
    """
    result = [initState]
    for i in range(1,time+1):
        vel = result[i-1][1] + acceleration
        pos = result[i-1][0] + vel 
        result.append((pos,vel))
    return result[1:]

# --- Lets set up the constants ---
        
# Lets say GPS position given in (km,km/h)
# H Transforms (pos,vel) from units of (km,km/h) to (m,m/s)
C = np.matrix([[1000,0],
               [0,1000/(60.*60.)]])
C_inv = np.linalg.inv(C)

#Identity (only need to make it once
I = np.matrix(np.identity(2),copy=False)

# This matrix converts the 2x2 matrix to the 2x2 Kalman Gain Matrix
# In this case it's not really needed, but is here for demonstration purposes
H = I

# The accelerometer output is already in m/s^2 rather than G's so no converison

#Defining this to demonstrate where it would go.
deltaT = 1

# State progression matrix
A = np.matrix([[1,deltaT],[0,1]]) #delta T is always 1 here
B = np.matrix([[0.5*deltaT*deltaT],[1*deltaT]])

#Process noise covariance matrix
Q = np.matrix([[0.2,0],[0,0.2]])

# GPS standard deviation coordinate error 
# for both position and velocity in m and m/s respectively
gps_error = 4

# Accelerometer Error (m/s^2)
accel_error = 0.5


class State:
    """
        This state class is useful for state progression as it also acts as a 
        linked list of all previous states. It makes the calculations more convenient.
    """
    def __init__(self,prev=None,init=(0,0),accel_data=0,init_gps_err=(gps_error,gps_error),init_accel_err=accel_error):
        #This Init contains the predict step, the prediction does take a measurement of acceleration data
        self.prev = prev
        self.accel = accel_data
        #Control Variable matrix
        Bu = B*self.accel 
        
        
        if self.prev is None:
            self.state = np.matrix(init).transpose()
            #We need our first measurement, in absence of this the estimation in just the following
            # P is the process covariance matrix (Error in Estimate)
            self.P = np.matrix([[4,0],[0,4]])
            
            #No previous state, so use these initial conditions
            self.P_est = A*self.P*A.transpose() + Q
            self.state_est = A*self.state + Bu
            
            #Measurement covariance matrix (Error is measurement)
            self.R = np.matrix([[init_gps_err[0]*init_gps_err[0],init_gps_err[0]*init_gps_err[1]],
                                [init_gps_err[1]*init_gps_err[0],init_gps_err[1]*init_gps_err[1]]])
        else:
            self.P_est = A*self.prev.P*A.transpose() + Q
            self.R = self.prev.R # R doesn't change but is defined by initial conditions
            
            self.state_est = A*self.prev.state + Bu
            
        self.KG = (self.P_est * H.transpose())/(H * self.P_est * H.transpose() + self.R)
        
        
    def measure(self, measurement=(0,0)):
        #This method contains the measurement and state update steps.
        #It also measures the acceleration to be used in the new states predict step.
        self.gps = measurement
        Y = C*np.matrix(measurement).transpose()
        
        #Update the current state.
        self.state = self.state_est + self.KG*(Y-H*self.state_est)
        self.P = (I-self.KG*H)*self.P_est
        
        return (tuple(self.state.reshape(1,2).A1),(tuple(self.P[0,:]),tuple(self.P[1,:])))
        
    def next(self, accel=0):
        return State(prev=self,accel_data=accel)
    
    def get_all_accel(self):
        if not hasattr(self,"accel") and hasattr(self.prev,"accel"):
            return self.prev.get_all_accel()
        tmp = [self.accel]
        if self.prev == None or self.prev.accel == None:
            return tmp
        tmp.extend(self.prev.get_all_accel())
        return tmp
    
    def get_all_gps(self):
        if not hasattr(self,"gps") and hasattr(self.prev,"gps"):
            return self.prev.get_all_gps()
        tmp = [self.gps]
        if self.prev == None or self.prev.gps == None:
            return tmp
        tmp.extend(self.prev.get_all_gps())
        return tmp
    
    def get_all_positions(self):
        if not hasattr(self,"state") and hasattr(self.prev,"state"):
            return self.prev.get_all_positions()
        tmp = [float(self.state[0])]
        if self.prev is None or self.prev.state is None:
            return tmp
        tmp.extend(self.prev.get_all_positions())
        return tmp
        
    def get_all_velocities(self):
        if not hasattr(self,"state") and hasattr(self.prev,"state"):
            return self.prev.get_all_velocities()
        tmp = [float(self.state[1])]
        if self.prev is None or self.prev.state is None:
            return tmp
        tmp.extend(self.prev.get_all_velocities())
        return tmp
    
    def get_all_Kalman_Gains(self):
        if not hasattr(self,"KG") and hasattr(self.prev,"KG"):
            return self.prev.get_all_Kalman_Gains()
        tmp = [self.KG.A1]
        
        if self.prev is None or self.prev.KG is None:
            return tmp
        tmp.extend(self.prev.get_all_Kalman_Gains())
        return tmp
    
    def to_vec(self):
        return self.state
    
    def __str__(self):
        """ Overloads the str() method, makes for easy visualization """
        tmp = self.state.transpose().A1
        return "Pos: %s - Vel: %s" % (tmp[0],tmp[1])
    
    def print_list(self):
        """ Prints the entire set of states """
        if self.prev==None:
            return ""
        return str(self)+"\n"+self.prev.print_list()
    
if __name__ == '__main__':
    
    #--- Now lets generate the real data ---
    
    # Modify this to modify the path of the train
    
    #real gps positions and velocities (m,m/s)
    gps_real = [(0,0)]
    #real accelerometer readings (m/s^2)
    accel_real = [0]
    #do nothing 5 seconds
    gps_real.extend(generate_gps((0,0),0,4))
    accel_real.extend([0]*4)
    #accelerate 2 m/s^2 for 10 seconds
    gps_real.extend(generate_gps(gps_real[-1],2,10))
    accel_real.extend([2]*10)
    #do nothing 10 seconds
    gps_real.extend(generate_gps(gps_real[-1],0,10))
    accel_real.extend([0]*10)
    #decelerate 1 m/s^2 for 20 seconds
    gps_real.extend(generate_gps(gps_real[-1],-1,20))
    accel_real.extend([1]*20)
    #do nothing 5 seconds
    gps_real.extend(generate_gps(gps_real[-1],0,5))
    accel_real.extend([0]*5)
    #decelerate 1 m/s^2 for 20 seconds
    gps_real.extend(generate_gps(gps_real[-1],-1,20))
    accel_real.extend([-1]*20)
    #decelerate 2 m/s^2 for 10 seconds
    gps_real.extend(generate_gps(gps_real[-1],-2,10))
    accel_real.extend([-2]*10)
    #accelerate 2 m/s^2 for 20 seconds
    gps_real.extend(generate_gps(gps_real[-1],2,20))
    accel_real.extend([2]*20)
    #do nothing for 40 seconds
    gps_real.extend(generate_gps(gps_real[-1],0,40))
    accel_real.extend([0]*40)
    #accelerate by 2 m/s^2 for 20 seconds
    gps_real.extend(generate_gps(gps_real[-1],1,20))
    accel_real.extend([1]*20)
    #do nothing for 35 seconds
    gps_real.extend(generate_gps(gps_real[-1],0,35))
    accel_real.extend([0]*35)
    #accelerate by 2 m/s^2 for 20 seconds
    gps_real.extend(generate_gps(gps_real[-1],-8,5))
    accel_real.extend([-8]*5)
    
    if len(gps_real) != len(accel_real):
        print("Both gps_real and accel_real should be the same length!")
        print("GPS: %d , Accel: %d" % (len(gps_real),len(accel_real)))
    
    #Accel takes a measurement for the predict state and so it is 1 step ahead
    #This ensures no crash upon the very last GPS measurement when it 
    #attempts to grap an extra bit of accel data.
    accel_real.append(0)
    
    # I reverse this list so I can easily just pop the values.
    gps_real.reverse()
    accel_real.reverse()
    
    #--- Generate the noisy measurement data ---
    
    #I'm converting the real values into noisy measured value
    gps_mea = []
    for i in gps_real:
        gps_mea.append(tuple(np.matmul(C_inv,make_gps_noisy(i, sd=gps_error)).A1))
        
    accel_mea = []
    for i in accel_real:
        accel_mea.append(i+noise(sd=accel_error))
    
    #Initial State Variables
    
    #states = [State(0,0)] #Initial state is 0,0
    #predicted_states = [State(0,0)] #Estimated initial state is spot on
    #errors = [np.identity(2)]
    #predicted_errors = [np.identity(2)]
    
    state = State(init=(0,0),accel_data=accel_mea.pop()) #Initial State pos=0,vel=0
    print("Initial Kalman Gain")
    print(state.KG)
    
    print("Simulating...")
    start_time = time.time()
    
    while gps_mea and accel_mea:
        state.measure(gps_mea.pop())
        state = state.next(accel_mea.pop())
        
    print("Simulation Complete "+str(time.time()-start_time)+"s")
    print("Generating plots...")
    
    x = list(np.arange(len(gps_real)))
    x.reverse()
    y = state.get_all_gps()
    real_positions = [a[0] for a in gps_real]
    real_velocities = [a[1] for a in gps_real]
    
    plt.plot(x,real_positions)
    plt.plot(x,[a[0]*1000 for a in y])
    plt.title("Real vs Noisy GPS Position over Time")
    plt.xlabel("Time (s)")
    plt.ylabel("Position (m)")
    plt.savefig("Real_vs_Noisy_GPS_Position")
    plt.close()
    
    plt.plot(x,real_velocities)
    plt.plot(x,[a[1]*(1000/(60.*60.)) for a in y])
    plt.title("Real vs Noisy GPS Velocity over Time")
    plt.xlabel("Time (s)")
    plt.ylabel("Velocity (m/s)")
    plt.savefig("Real_vs_Noisy_GPS_Velocity.png")
    plt.close()
    
    plt.plot(x,accel_real[1:])
    plt.plot(x,state.get_all_accel()[1:])
    plt.title("Real vs Noisy Acceleration over Time")
    plt.xlabel("Time (s)")
    plt.ylabel("Position (m/s^2)")
    plt.savefig("Real_vs_Noisy_Acceleration.png")
    plt.close()
    
    z = state.get_all_Kalman_Gains()[1:]
    plt.plot(x,[a[0] for a in z],label="Top Left")
    plt.plot(x,[a[1] for a in z],label="Top Right")
    plt.plot(x,[a[2] for a in z],label="Bottom Left")
    plt.plot(x,[a[3] for a in z],label="Bottom Right")
    plt.title("Kalman Gain over Time")
    plt.xlabel("Time (s)")
    plt.ylabel("Kalman Gain")
    plt.legend()
    plt.savefig("Kalman_Gains.png")
    plt.close()
    
    y = state.get_all_positions()
    plt.plot(x,real_positions)
    plt.plot(x,y)
    plt.title("Real vs Predicted Position over Time")
    plt.xlabel("Time (s)")
    plt.ylabel("Position (m)")
    plt.savefig("Real_vs_Predicted_Position.png")
    plt.close()
    
    y = state.get_all_velocities()
    plt.plot(x,real_velocities)
    plt.plot(x,y)
    plt.title("Real vs Predicted Velocity over Time")
    plt.xlabel("Time (s)")
    plt.ylabel("Velocity (m/s)")
    plt.savefig("Real_vs_Predicted_Velocity.png")
    plt.close()
    
    print("Plots generated, process complete.")
    
    